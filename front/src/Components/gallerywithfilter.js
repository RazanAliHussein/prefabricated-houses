import React, { Component } from "react";
import "./Style/style.css";
import { Link } from "react-router-dom";
import "./Style/gallery.css";

var filter = "all";

 const Store = ({ category_id, price, img,product_id }) => {
  return (
    <div className="galleries">
      <figure className="effect-goliath">
        <img src={require("./images/g1.jpg")} alt="img15" />
        <figcaption>
          <h6>{category_id}</h6>
          <h5>{price}</h5>
          <Link to={`/gallery/item/${product_id}`} className="btn click-me" >
            more..
          </Link>
        </figcaption>
      </figure>
    </div>
  );
};

class Gallery extends Component {
  state = {
    listSelected: "all",
    products_list: []
  };
  renderList = () => {
    if (this.state.listSelected === "all") {
      return (
        <div>
          <h1>All:</h1>
          {this.state.products_list.map(product => (
          <Store
            className="Gallery-Item"
            key={product.product_id}
            type={product.category_id} // All - 0 - 1 - 2 - 3 < 6
            price={product.product_price}
            product_id={product.product_id}
            // img = {"public/images/default/350x250.png"}
            // product.product_path
          />
        ))}
          
        </div>
      );
    }
     else if (this.state.listSelected === "M1") {
     const M1 = this.state.products_list.filter(function(user) {
        return user.category_id === 1;
      });
      return (
        <div>
          <h1>M1</h1>
          {M1.map(product => (
          <Store
            className="Gallery-Item"
            key={product.product_id}
            type={product.category_id} // All - 0 - 1 - 2 - 3 < 6
            price={product.product_price}
            product_id={product.product_id}
            // img = {"public/images/default/350x250.png"}
            // product.product_path
          />
        ))}
          
        </div>
      )
    
    }
     else if (this.state.listSelected === "M3") {
      const M2 = this.state.products_list.filter(function(user) {
        return user.category_id === 2;
      });
      return (
        <div>
          <h2>M3</h2>
          {M2.map(product => (
          <Store
            className="Gallery-Item"
            key={product.product_id}
            type={product.category_id} // All - 0 - 1 - 2 - 3 < 6
            price={product.product_price}
            product_id={product.product_id}
            // img = {"public/images/default/350x250.png"}
            // product.product_path
          />
        ))}
        </div>
      );
    } 
    else if (this.state.listSelected === "M5") {
      const M5 = this.state.products_list.filter(function(user) {
        return user.category_id === 5;
      });
      return (
        <div>
          <h2>M5</h2>
          {M5.map(product => (
          <Store
            className="Gallery-Item"
            key={product.product_id}
            type={product.category_id} // All - 0 - 1 - 2 - 3 < 6
            price={product.product_price}
            product_id={product.product_id}
            // img = {"public/images/default/350x250.png"}
            // product.product_path
          />
        ))}
        </div>
      );
    }
    else {
      return <div>Error</div>;
    }
  };
  FilterAll = () => {
   
    this.setState({ listSelected: "all" });
  
  };
  FilterM1 = () => {
    this.setState({ listSelected: "M1" });
  

  };

  FilterM3 = () => {
    this.setState({ listSelected: "M3" });
 
  };
  FilterM5 = () => {
    this.setState({ listSelected: "M5" });
 
  };

  getStoreProducts = async () => {
    this.setState({ isLoading: true });
    try {
      const response = await fetch(
        `http://localhost:8080/store/products/?filter=${filter}`
      );
      const answer = await response.json();
      if (answer.success) {
        const products_list = answer.result;
        this.setState({ products_list, isLoading: false });
      } else {
        this.setState({ error_message: answer.message, isLoading: false });
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading: false });
    }
  };
  componentDidMount() {
    this.getStoreProducts();
  }

  render() {
    console.log(this.state.listSelected)
    return (
      <div className="Gallery">
        {/* gallery */}
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        products_list
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <div className="title ">
          <h3>Our Gallery</h3>
        </div>
        <div>
      
            <button className="btn click-me" onClick={this.FilterAll}>
              All
            </button>
            <button className="btn click-me" onClick={ this.FilterM1}>
              M1
            </button>
            <button className="btn click-me" onClick={this.FilterM3}>
              M3
            </button>
            <button className="btn click-me" onClick={this.FilterM5}>M5</button>
         
        </div>
        {/* {this.state.products_list.map(product => (
          <Store
            className="Gallery-Item"
            key={product.product_id}
            type={product.category_id} // All - 0 - 1 - 2 - 3 < 6
            price={product.product_price}
            // img = {"public/images/default/350x250.png"}
            // product.product_path
          />
        ))} */}
        {/* Gallery	 */}
        {this.renderList()}
      </div>
    );
  }
}
export default Gallery;
