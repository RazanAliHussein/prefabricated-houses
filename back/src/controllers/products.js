import Express from 'express'

const app = Express()

export default async (controller) => {
  app.get('/', (req, res, next) => res.send("ok from products"));

  // Get product By ID 
  // Route : /store/product/1,2,3..
  app.get('/store/product/:id', async (req, res, next) => {
    try {
      const {
        id
      } = req.params
      const contact = await controller.getProduct(id);
      res.json({
        success: true,
        result: contact
      })
    } catch (e) {
      next(e)
    }
  });

  // Get All Products
  // Route : /store/products?filter=all
  app.get('/store/products/', async (req, res, next) => {
    try {
      const {
        filter
      } = req.query;

      const products = await controller.getProductsByCat(filter);
      res.json({
        success: true,
        result: products
      })
    } catch (e) {
      next(e)
    }
  });

  // New Product
  // Route : /product/new?title=value&description=value&price=value&category_id=0
  app.get("/product/new", async (req, res, next) => {
    try {
      const {
        title,
        description,
        price,
        category_id
      } = req.query;
      const result = await controller.createProduct({
        title,
        description,
        price,
        category_id
      });
      res.json({
        success: true,
        result
      });
    } catch (e) {
      next(e);
    }
  });

  // Product Images - TEMP May the lord never see this.
  // ROUTE : /product/1/images/
  app.get('/product/:id/images/', async (req, res, next) => {
    try {
      const {
        id
      } = req.params
      const productimg = await controller.getProductImages(id);
      res.json({
        success: true,
        result: productimg
      });
    } catch (e) {
      next(e);
    }
  });

  // DELETE PRODUCT || @TODO handle IMAGE DELETE TOO
  // Route : /product/delete/1,2,3..
  app.get("/product/delete/:id", async (req, res, next) => {
    try {
      const {
        id
      } = req.params;
      const result = await controller.deleteProduct(id);
      res.json({
        success: true,
        result
      });
    } catch (e) {
      next(e);
    }
  });

  // UPDATE PRODUCT || @TODO CALL IMAGE TOO
  // Route : /product/update/1?title=value&description=value&price=value&category_id
  app.get("/product/update/:id", async (req, res, next) => {
    try {
      const {
        id
      } = req.params;
      const {
        title,
        description,
        price,
        category_id
      } = req.query;
      const result = await controller.updateProduct(id, {
        title,
        description,
        price,
        category_id
      });
      res.json({
        success: true,
        result
      });
    } catch (e) {
      next(e);
    }
  });

  return app;
}