import React, { Component } from "react";
import "./Style/style.css";
import { Link } from "react-router-dom";
import "./Style/contact.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStroopwafel } from "@fortawesome/free-solid-svg-icons";
import {
  faTwitter,
  faFacebook,
  faInstagram
} from "@fortawesome/fontawesome-free-brands";
library.add(faStroopwafel);

class Header extends Component {
  render() {
    return (
      <div className="Header">
       <div className="header-outs" id="header">
          {/* banner */}
          <div className="header-most-top">
            <div className="one-headder">
              <div className="container-fluid">
                <div className="row left-indus-icons RWDpagescrollfix">
                  <div className="col-lg-5 col-md-5 col-sm-4 pr-0 icons">
                    <ul>
                      <li>
                        <h4> Follow Us:</h4>
                      </li>
                      <li>
                        <a href="https:www.facebook.com">
                          <FontAwesomeIcon
                            className="font-awesome"
                            icon={faFacebook}
                          />
                        </a>
                      </li>
                      <li>
                        <a href="https:www.instagram.com">
                          <FontAwesomeIcon
                            className="font-awesome"
                            icon={faInstagram}
                          />
                        </a>
                      </li>
                      <li>
                        <a href="https:www.twitter.com">
                          <FontAwesomeIcon
                            className="font-awesome"
                            icon={faTwitter}
                          />
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-lg-7 col-md-7 col-sm-8 agile-email-call ">
                    <ul>
                      <li>
                        <h4>Call Us:</h4>
                      </li>
                      <li>
                        <p>24/7</p>
                      </li>
                      <li>
                        <h4>Email Us:</h4>
                      </li>
                      <li>
                        <p>
                          <a href="mailto:info@example.com">
                            info@example1.com
                          </a>
                        </p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="headder-nav-icon pagescrollfix">
              <div className="container-fluid">
                <div className="nav-headder-top">
                  <nav className="navbar navbar-expand-lg navbar-light pagescrollfix">
                    <div className="hedder-up">
                      <h1>
                        <a className="navbar-brand" href="index.html">
                          <span className="fas fa-cogs" />Ready set Homes
                        </a>
                      </h1>
                    </div>

                    <div
                      className="collapse navbar-collapse justify-content-end"
                      id="navbarSupportedContent"
                    >
                      <ul className="navbar-nav ">
                        <li className="nav-item active">
                          <Link to="/" className="nav-link">
                            Home <span className="sr-only" />
                          </Link>
                        </li>
                        <li className="nav-item">
                          <Link to="/about" className="nav-link">
                            About
                          </Link>
                        </li>

                        <li className="nav-item">
                          <Link to="/gallery" className="nav-link">
                            Gallery
                          </Link>
                        </li>
                        <li className="nav-item">
                          <Link to="/blog" className="nav-link">
                            News & Blogs
                          </Link>
                        </li>
                        <li className="nav-item">
                          <Link to="/contact" className="nav-link">
                            Contact
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </nav>
                </div>
                <div className="clearfix"> </div>
              </div>
            </div>
          </div>
        </div>
     
      </div>
    );
  }
}
export default Header;
