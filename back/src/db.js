import sqlite from 'sqlite'
import SQL from 'sql-template-strings';
import { IncomingMessage } from 'http';
import { EPROTONOSUPPORT } from 'constants';

const CategoryLimit = 6;

function getDateToday()
{
  let today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1; //January is 0!
  const yyyy = today.getFullYear();

  dd = dd < 10 ? `0${dd}` : dd;
  mm = mm < 10 ? `0${mm}` : mm;

  today = mm + '/' + dd + '/' + yyyy;
  return today;
}

const initializeDatabase = async () => {
  const path = require('path');
  const dbPath = path.resolve(__dirname, '../database/db.db');
  const db = await sqlite.open(dbPath);

  // SLIDER
  const getSliderData = async () => {
    try {
      const sd = await db.all(SQL `SELECT images.image_path, images.image_width, images.image_height,
        slider.slider_text, slider.slider_title
        FROM slider
        INNER JOIN images
        ON images.image_id = slider.image_id`);
      if (!sd)
        throw new Error(`no slider data found`);
      return sd;
    } catch (e) {
      throw new Error(`couldn't retrieve slider data: ${e.message}`);
    }
  }
  // USERS

  // blog/articles
  const getArticles = async (id = 0) => {
    try {
      let statement = SQL`SELECT article_id, article_title, article_text, article_date, article_owner,
                      image_path,  image_width, image_height FROM articles
                      INNER JOIN images
                      WHERE articles.deleted = 0 and articles.image_id = images.image_id`;
      if (id != 0) {
        statement.append(SQL` AND article_id = ${id}`);
      }
      const articles = await db.all(statement);
      if (!articles)
        throw new Error(`no articles data found`);

      return articles;
    }
    catch (e) {
      throw new Error(`couldn't retrieve articles data: ${e.message}`);
    }
  }
  // Invalid can't articles doesn't load if no images found.
  // load all images and save in memory instead, collect the image from the memory and refresh it in case of new upload
  // const getArticles = async(id = 0) => {
  //   try{
  //     let statement = SQL`SELECT articles.article_id, articles.article_title, articles.article_text, articles.article_date, articles.article_owner,
  //     images.image_path, images.image_width, images.image_height
  //     FROM articles
  //       INNER JOIN images
  //     ON images.image_id = images_articles.image_id
  //       INNER JOIN images_articles
  //     ON articles.article_id = images_articles.article_id
  //     WHERE articles.deleted = 0`;
  //     if (id > 0) {
  //       statement += ` AND articles.article_id = ${id}`;
  //     }

  //     const imgs = await db.all(statement);
  //     if (!imgs)
  //       throw new Error(`Post images at ${id} not found`);
  //     return imgs;
  //   }catch (e) {
  //     throw new Error(`can't retrieve images for this post ${id} : ${e.message}`);
  //   }
  // }
  // PAGES TEXT

  // GALLERY
  const getCategories = async () => {
    try {
      const categoryList = await db.all(SQL `SELECT * FROM category`);
      if (!categoryList) {
        throw new Error(`no categories found`);
      }
      return categoryList;
    } catch (e) {
      throw new Error(`couldn't retrieve categories: ${e.message}`);
    }
  }

  const getProductImages = async(id) => {
    try{
      const imgs = await db.all(
        SQL`SELECT images.*,  images_products.def FROM images
        INNER JOIN images_products
        ON images_products.image_id = images.image_id
        WHERE  images_products.product_id = 1
        ORDER BY images_products.def DESC`);
      if (!imgs)
        throw new Error(`product images ${id} not found`);
      return imgs;
    }catch (e) {
      throw new Error(`can't retrieve images for this product ${id} : ${e.message}`);
    }
  }

  const getProduct = async (id) => {
    try{
      const productList = await db.all(SQL`SELECT * FROM products WHERE product_id = ${id}`);
      const product = productList[0]
      if(!product){
        throw new Error(`product ${id} not found`)
      }
      return product
    }catch(e){
      throw new Error(`couldn't get the product ${id}: ${e.message}`);
    }
  }

  const getProductsByCat = async (orderBy) => {
    try {
      const statement = SQL`SELECT products.*,
                      images.image_path, images.image_width, images.image_height FROM images
                      INNER JOIN  products
                      ON products.product_id = images_products.product_id
                      INNER JOIN images_products
                      ON images_products.image_id = images.image_id
                      WHERE images_products.def = 1`;
      if (Math.abs(orderBy) < CategoryLimit) { // may the lord never see this...
        statement.append(SQL` AND products.category_id = ${orderBy}`);
      } else if (orderBy !== 'all')
        throw new Error(`no filter found for category_id ${orderBy}`);

      const rows = await db.all(statement);
      if (!rows.length) {
        throw new Error(`no products found`)
      }
      return rows

    } catch (e) {
      throw new Error(`couldn't retrieve products: ${e.message}`)
    }
  }

  // CREATE
  // Articles
  const createArticles = async (props) => {
    console.log({props});
    if(!props || !props.title || !props.text || !props.owner || !props.image_id){
      throw new Error(`you must provide all informations`)
    }
    const date = getDateToday();
    const { title, text, owner, image_id } = props;
    try{
      const result = await db.run(SQL`INSERT INTO articles
      (article_title, article_text, article_date, article_owner, image_id) VALUES
      (${title}, ${text}, ${date}, ${owner}, ${image_id})`);
      const id = result.stmt.lastID; // ? I don't understand this
      return id; // Why do we return ?
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
  // Products
  const createProduct = async (props) => {
    if(!props || !props.title || !props.description || !props.price || !props.category_id){
      throw new Error(`you must provide all informations`)
    }
    const { title, description, price, category_id } = props;
    try{ // ToDo
      const result = await db.run(SQL`INSERT INTO products
      (product_title, product_description, product_price, product_stock, category_id) VALUES
      (${title}, ${description}, ${price}, '1', ${category_id})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
  // Slider
  const createSlider = async (props) => {
    if(!props || !props.image_id || !props.title | !props.text){
      throw new Error(`you must provide all informations`)
    }
    const { image_id, title, text } = props
    try{
      const result = await db.run(SQL`INSERT INTO slider
      (image_id, slider_title, slider_text) VALUES
      (${image_id}, ${title}, ${text})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
  // DELETE
  const deleteArticle = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM articles WHERE article_id = ${id}`);
      if(result.stmt.changes === 0){
        throw new Error(`article_id "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the contact "${id}": `+e.message)
    }
  }
  const deleteProduct = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM products WHERE product_id = ${id}`);
      if(result.stmt.changes === 0){
        throw new Error(`product "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the product "${id}": `+e.message)
    }
  }
  const deleteSlider = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM slider WHERE slider_id = ${id}`);
      if(result.stmt.changes === 0){
        throw new Error(`slider "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the slider "${id}": `+e.message)
    }
  }

  // UPDATE
  // Article
  const updateArticle = async (id, props) => {
    if(!props || !(props.title || props.text || props.image_id)) {
      throw new Error(`you must provide all informations`);
    }
    const { title, text, image_id } = props;
    const date = 'Updated - ' + getDateToday();
    try {
      let statement = SQL`UPDATE articles SET
      article_title=${title}, article_text=${text}, article_date=${date}, image_id=${image_id}
      WHERE article_id = ${id}`;

      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the Article ${id}: ` + e.message);
    }
  }
  //
  const updateProduct = async (id, props) => {
    if (!props || !props.title || !props.description || !props.price || !props.category_id) {
      throw new Error(`you must provide a blah or an blahblah`);
    }
    const { title, description, price, category_id } = props;
    try {
      let statement = SQL`UPDATE products SET
      product_title=${title}, product_description=${description}, product_price=${[price]}, category_id=${category_id}
      WHERE product_id = ${id}`;
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the Product ${id}: ` + e.message);
    }
  }
  // Slider
  const updateSlider = async (id, props) => {
    if (!props || !props.image_id || !props.title | !props.text) {
      throw new Error(`you must provide all informations`);
    }
    const { image_id, title, text } = props;
    try {
      let statement = statement = SQL`UPDATE slider SET
       image_id=${image_id}, slider_title=${title}, slider_text=${text}
       WHERE slider_id = ${id}`;
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the Slider ${id}: ` + e.message);
    }
  }

  // IMAGE HANDLERS
  const createImage = async (props) => {
    const {
      width,
      height,
      image
    } = props;
    try {
      const listOfID = image.map(async x => {
        // Probably not the best query of the world. build single insert instead..
        const result = await db.run(SQL `INSERT INTO images (image_width, image_height, image_path) VALUES
          (${width}, ${height}, ${x.path})`);

        return result.stmt.lastID;
      });
      const ids = await Promise.all(listOfID);
      
      return ids;
    } catch (e) {
      throw new Error(`Image uploads failed, please try again $error : ${e}`);
    }
  }

  // Create Products Images
  const createProductImages = async (props) => {
    if(!props || !props.imgID || !props.productID || !props.def)
    {
      throw new Error(`you must provide all informations`)
    }
    try {
      const {
        imgID,
        productID,
        def,
      } = props;

      // Probably not the best query of the world. build single insert instead..
      const result = await db.run(SQL`INSERT INTO images (image_id, product_id, def) VALUES
        (${imgID}, ${productID}, ${def})`);

      const id = result.stmt.lastID;
      return id;
    }
    catch (e) {
      throw new Error(`$error : ${e}`);
    }
  }

  // Cart system
  const addCartDataForUser = async (props) => {
    if(!props || !props.userID || !props.prodID)
    {
      throw new Error(`you must provide all informations`)
    }
    try {
      const {
        userID,
        prodID
      } = props;
      
      // Probably not the best query of the world. build single insert instead..
      const result = await db.run(SQL`INSERT INTO user_cart (cart_userID, cart_productID) VALUES
        (${userID}, ${prodID})`);
        console.log(result);
      const id = result.stmt.lastID;
      return id;
    }
    catch (e) {
      throw new Error(`$error : ${e}`);
    }
  }

  const getCartDataForUser = async (userID) => {
    console.log(userID);
    if (!userID)
    {
      throw new Error(`you must provide all information`);
    }
    try {
    const result = await db.all(SQL`SELECT user_cart.cart_productID, products.product_title, products.product_price FROM user_cart
    INNER JOIN products
    ON
    products.product_id = user_cart.cart_productID
    WHERE  user_cart.cart_userID = ${userID}`);
    if (!result)
      throw new Error(`Cart Empty for USER ${userID} or not found`);
    return result;

    }catch (e){
      throw new Error(`$error : ${e}`);
    }
  }

  const deleteCartData = async (userID, props) => {
    if (!props || !props.prodID)
        throw new Error(`you must provide all informations`);
    try {
      const {
        prodID
      } = props;
      const result = await db.run(SQL`DELETE FROM user_cart 
      WHERE cart_userID = ${userID} AND cart_productID = ${prodID}`);
      if(result.stmt.changes === 0){
        throw new Error(`Can't delete cart info for userID = ${userID}`)
      }
      return true;
    } catch(e){
      throw new Error(`$error : ${e}`)
    }
  }

  const controller = {
    // Slider
    getSliderData,
    // Galery/Store
    getCategories, // Filter
    getProduct,
    getProductsByCat,
    getProductImages, // TODO <> Unefficient handling
    // Articles
    getArticles,
    //getPostImage, // TODO <> Hackfix direct link article 1 image max

    // Create
    createArticles,
    createProduct,
    createSlider,
    // Delete
    deleteArticle,
    deleteProduct,
    deleteSlider,
    // Update
    updateArticle,
    updateProduct,
    updateSlider,
    createImage,
    createProductImages,
    addCartDataForUser,
    getCartDataForUser,
    deleteCartData
  }

  return controller
}


export default initializeDatabase
