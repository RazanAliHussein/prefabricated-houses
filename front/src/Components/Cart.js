import React, {Component} from 'react';
import "./Style/cart.css";

// const CartItem = ({name,price, id})=> {
//   return(
//     <tr>
//     <td>{name}</td>
//     <td>{price + "$"}</td>
//     <td>
//     <button onClick={() => (this.deleteOrder(1))}>
//     {console.log("DEBUG" + id)}
//       Delete
//     </button>
//     </td>
//     </tr>
//   )
// }

class Cart extends Component {
  state = {
    user_cart : [],
    id:""
 }

 deleteItem = async id => {
  try {
  console.log(`DEBUG FROM DELETEORDER + ${id}`);
    const response = await fetch(
      `http://localhost:8080/orders/delete/1?prodID=${id}`
    );
    window.alert("Item deleted");
    const answer = await response.json();
    if (answer.success) {
      // remove the user from the current list of users
      const user_cart = this.state.user_cart.filter(
        cart => cart.id !== id
      );
      this.setState({ user_cart });
    } else {
      this.setState({ error_message: answer.message });
    }
  } catch (err) {
    this.setState({ error_message: err.message });
  }
};


 getCartForUser = async () => {
    this.setState({ isLoading: true });
    try {
      const response = await fetch(
        `http://localhost:8080/orders/data/1` // Grab the user ID
      );
      const answer = await response.json();
      if (answer.success) {
        const user_cart = answer.result;
        this.setState({ user_cart, isLoading: false });
      } else {
        this.setState({ error_message: answer.message, isLoading: false });
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading: false });
    }
  };
  componentDidMount() {
    this.getCartForUser();
  }

  render() {
    return(
      <div>
        <br/><br/><br/><br/><br/><br/><br/><br/> <br/><br/><br/><br/><br/><br/><br/>
      <h1>Cart</h1>
      <form>

    <table>
    <tr>
    <th>Product Title</th>
    <th>Price</th>
    <th>Action</th>
    </tr>
  
    {this.state.user_cart.map(x => 
      // <CartItem key= {x.id}
      // name= {x.product_title}
      // price= {x.product_price} 
      // // Create a button here to delete the by key 
      // />
      <tr>
      <td>{x.product_title}</td>
      <td>{x.product_price + "$"}</td>
      <td>
      <button onClick={() => this.deleteItem(x.cart_productID)}>
      {console.log("DEBUG" + this.state.id)}
        Delete
      </button>
      </td>
      </tr>
    )}
  </table>
  </form>
  </div>
    )
  }
}

export default Cart;