import multer from 'multer';

const formatDate = (str) =>
{
    return str.replace("T", "_").replace("Z", "_").replace(/:/g, "_").replace(/-/g, "_").replace('.', "_");
}

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, 'public/uploads');
  },
  filename: function(req, file, cb) {
    var fname = file.originalname;
    const date = new Date().toISOString();
    cb(null, `${formatDate(date)}${fname.replace(/ /g, '_')}`);
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

export default upload