import Express from 'express'

const app = Express()

export default async (controller) => {
  app.get('/', (req, res, next) => res.send("ok from cart"));

  // Get All Cart Data for user
  // Route : /cart/data
  app.get('/data/:id', async (req, res, next) => {
    try {
      const {
        id
      } = req.params;

      const data = await controller.getCartDataForUser(id);
      res.json({
        success: true,
        result: data
      });
    } catch (e) {
      next(e);
    }
  });

  // DELETE CART PRODUCT
  // /cart/delete/1,2,3 ? RowID | Cient + Product ?
  app.get("/delete/:id", async (req, res, next) => {
    try {
      const {
        id
      } = req.params;
      const { 
        prodID
      } = req.query;
      const result = await controller.deleteCartData(id, { prodID });
      res.json({
        success: true,
        result
      });
    } catch (e) {
      next(e);
    }
  });

  // CREATE NEW CART 
  // /cart/new?userID=value&prodID=value
  app.get("/new", async (req, res, next) => {
    try {
      const {
        userID,
        prodID,
      } = req.query;
      const result = await controller.addCartDataForUser({
        userID,
        prodID,
      });
      res.json({
        success: true,
        result
      });
    } catch (e) {
      next(e);
    }
  });

  return app;
}