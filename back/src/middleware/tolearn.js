  // okay to debug
  // Move this to single place |Middleware| Cleanup things || What if there's no props ?
  const tryCatch = (method,argsNames, checker = ()=>true) => async (req,res,next) =>{
    try {
      const props = {}
      if (argsNames) {
        Object.keys(argsNames).forEach(key => { // query, files, body...
          const params = argsNames[key] // [x,y,z]
          const repository = req[key]
          console.log("DEBUG ");
          params.forEach(paramName => {
            const value = repository[paramName]
            console.log("DEBUG " + value);
            if (value !== null && typeof (value) !== 'undefined') {
              props[paramName] = value
            }
          })
        })
        const valid = checker(props)
        if (!valid) {
          return next(new Error('props is not valid'))
        }
        req.result = await controller[method](props);
        next()
      }
      else
      {
        req.result = await controller[method];
        next()
      }
    } catch (e) {
      next(e);
    }
  }

  const resultPicker = (propName) => (req, res, next) =>  {
    const answer = propName ? req.result[propName] : req.result
    res.json({ success: true, result: answer })
  }
  
  // Sample
  // // Insert Images Product 
  // app.get("/images/product/new", 
  // tryCatch('createProductImages',{query:['imgID','prodID', 'def']}), 
  //   resultPicker('lastID')
  // );


  // Fix probs sending array to the thing
  // app.post("/multi",
  //   upload.array('image', upload_limit), 
  //   tryCatch('createImage',{query:['width','height'], files:['image']}), 
  //   resultPicker()
  // );

  // // Single Post Image
  // app.post("/single", 
  //   upload.single('image'), 
  //   tryCatch('createImage',{query:['width','height'], file:['image']}), 
  //   resultPicker('lastID')
  // );

  // // Insert Images Product 
  // app.get("/images/product/new", 
  // tryCatch('createProductImages',{query:['imgID', 'prodID', 'def']}), 
  //   resultPicker('lastID')
  // );