import Express from 'express'

const app = Express()

export default async (controller) => {
  app.get('/', (req, res, next) => res.send("ok from blog"));

  // Get All Articles 
  // Route : /blog/articles
  app.get('/articles', async (req, res, next) => {
    try {
      const articles = await controller.getArticles();
      res.json({
        success: true,
        result: articles
      })
    } catch (e) {
      next(e);
    }
  });

  // Get Single Article
  // Route : /blog/articles/1, 2, 3...
  app.get('/articles/:id', async (req, res, next) => {
    try {
      const { id } = req.params;
      const article = await controller.getArticles(id);
      res.json({
        success: true,
        result: article
      })
    } catch (e) {
      next(e);
    }
  });

  // Create article
  // Route : /blog/article/new?title=Title&text=DDD&owner=Admin&image_id=0
  app.get("/article/new", async (req, res, next) => {
    try {
      const { title, text, owner, image_id } = req.query;
      const result = await controller.createArticles({ title, text, owner, image_id });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // Delete article
  // Route : /blog/article/delete/1,2,3...
  app.get("/article/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteArticle(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // UPDATE ARTICLE
  // Route : /blog/article/update/1?title=value&text=value&owner=value&image_id=0
  app.get("/article/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { title, text, owner, image_id } = req.query;
      const result = await controller.updateArticle(id, { title, text, owner, image_id });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  return app;
}