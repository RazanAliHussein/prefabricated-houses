import React, { Component } from 'react';
import Home from './Components/Home';
import {Link} from 'react-router-dom';
import Blog from './Components/Blog';
import About from './Components/about';
import Contact from './Components/contact';
import Gallery from './Components/gallery';
import Item from './Components/Item.js';
import {BrowserRouter as Router,Route} from "react-router-dom";
import './App.css';
/* DASHBOARD */
import Dash from './Dashboard/dash.js';
/* END OF DASHBOARD */
import Header from './Components/header.js';
import Footer from './Components/footer.js';
import OnetemtemBlog from './Components/OneBlog.js';
import './Components/Style/style.css';
import CartImage from '../src/Components/images/cart.png';
import Cart from './Components/Cart';
import Pagination from './Components/PaginationTest';
import Filter from './Components/gallerywithfilter';

// http://localhost:8080/login?username=nina.williams@tek.ken&password=hapkido

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
      <Header/>

    <Route exact path="/" component={Home} />  
		<Route path="/about" component={About} />
		<Route exact path="/gallery" component={Filter} />
		<Route exact path="/blog" component={Pagination} />
		<Route path="/contact" component={Contact} />
    <Route path="/gallery/item/:product_id" render={(props) =><Item global={global} {...props}/>} />
    <Route  path="/dashboard" component={Dash}/>
    <Route path ="/blog/one" component={OneBlog}/>
    <Route path ="/cart" component={Cart}/>
      
    
     <Footer/>
     <div id="Cartbutton">
      <Link to="/cart"><button><img className="Cart" src={CartImage}/></button>
      </Link>
    </div>
      </div>
      </Router>     
    );
  }
}

export default App;
