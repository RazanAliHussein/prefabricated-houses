﻿ Prefab Houses

1) Project Name 

prefabricated houses

2) Project Deadline 
one month

3) Overview 

Mr. Omar is the owner of ready set homes company which build Prefabricate houses. Ready Set Home is an architecture company that designs custom made prefabricated houses using premium materials. It has been operating since 1997.
IT has contacts in 3 countries Lebanon, France, and Canada. He has 5 types of houses M1, M3, M5, M9, and Villa He needs a website to show his work.  

4) Goal 

- Develop a user friendly website to display Ready set home's services and allow users to contact Ready Set Home company. 

5)Pages 

The below pages should be accessible in the menu:
*Home

*About us

*Gallery

*Services

*Contact us

#### Home 

- Front Page 
- Slider with button that links to services 
- Who are we 
- Team


#### Services

 Each service aligned with an image 
  
 We will provide it latter

#### About 

- Team 
- Values and Principles 
- Partners 

####Gallery

 -Images about the houses’s types

#### Contact 

- Contact Form 

- Contact email: 

Canada Contact: +1-860-770-9810 
info@readysethome.ca
France Contact: +843 305 0099 
info@readysethome.fr
Lebanon Contact: +96101183943
info@readysethome.lb


