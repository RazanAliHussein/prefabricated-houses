import './Style/style.css';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import T1 from "./images/t1.jpg";
import T2 from "./images/t2.jpg";
import T3 from "./images/t3.jpg";
import T4 from "./images/t4.jpg";
import g1 from './images/g1.jpg';
import g2 from './images/g2.jpg';
import g3 from './images/g3.jpg';
import Slide1 from './images/Slide1.jpeg';
import Slide2 from './images/Slide2.jpeg';
import Slide3 from './images/Slide3.jpeg';
import next from './images/next.png';
import { Fade } from 'react-slideshow-image';


class Home extends Component {
  state={
     fadeImages : [
      {Slide1}.Slide1,
      {Slide2}.Slide2,
      {Slide3}.Slide3
    ],
    fadeProperties : {
      duration: 5000,
      transitionDuration: 500,
      infinite: true,
      indicators: true,
      arrows: true
    }
  }
   Slideshow = () => {
    return (
      <Fade {...this.state.fadeProperties} className="slider callbacks_container">
      <div className="each-fade SlideShowContainer" >
        <div className="image-container container slider-img ">
          <img className="each-slide" src={this.state.fadeImages[0]} />
         
        </div>
        <div className="DataInsideSlider">
        <h2 className="TextInsideImage">Design custom made prefabricated houses</h2>
         <Link to="/gallery"><button className="ButtonInsideImage">Show more</button></Link> 
          </div>
      </div>
      <div className="each-fade container">
        <div className="image-container container slider-img ">
          <img className="each-slide" src={this.state.fadeImages[1]} />
        </div>
        <div className="DataInsideSlider">
        <h2 className="TextInsideImage">Design custom made prefabricated houses</h2>
        <Link to="/gallery"> <button className="ButtonInsideImage">Show more</button></Link>
        </div>
      </div>
      <div className="each-fade container">
        <div className="image-container container slider-img ">
          <img className="each-slide" src={this.state.fadeImages[2]} />
        </div>
        <div className="DataInsideSlider">
        <h2 className="TextInsideImage">Design custom made prefabricated houses</h2>
        <Link to="/gallery"><button className="ButtonInsideImage">Show more</button></Link>
        </div>
      </div>
    </Fade>)}
    render() {
      return (
        <div className="App">
            <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
                        <this.Slideshow/>
                        {/* Slide Show fade */}
          
   
          {/* <!-- //Navigation -->
                       <!-- Slideshow 4 --> */}
                      
                       <div>
                       <div className="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">
              <h3>Who we are</h3>
            </div>  
            <h1>Ready Set Home is an architecture company that designs custom made prefabricated houses using premium materials
            <br/>We've been operating since 1997</h1>   </div>     

          {/* top 3 */}
          <div className="tophouses">
            <section className="gallery py-lg-4 py-md-3 py-sm-3 py-3">
         <div className="container py-lg-5 py-md-4 py-sm-4 py-3">
            <div className="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">
               <h3>Our Gallery</h3>
            </div>
            <div className="row grid gallery-info">
               <div className="col-lg-4 col-md-6 col-sm-6 gallery-grids ">
                  <figure className="effect-goliath">
                     <img src={g1} alt="img15"/>
                     <figcaption>
                        <h6>Molten<span>Work</span></h6>
                        <p>Duis posuere ex in mollis iaculis</p>
                        <a href={g1} className="gallery-box" data-lightbox="example-set" data-title="">
                        </a>
                     </figcaption>
                  </figure>
               </div>
               <div className="col-lg-4 col-md-6 col-sm-6 gallery-grids">
                  <figure className="effect-goliath">
                     <img src={g2} alt="img15"/>
                     <figcaption>
                        <h6>Molten<span>Work</span></h6>
                        <p>Duis posuere ex in mollis iaculis</p>
                        <a href={g2} className="gallery-box" data-lightbox="example-set" data-title="">
                        </a>
                     </figcaption>
                  </figure>
               </div>
               <div className="col-lg-4 col-md-6 col-sm-6 gallery-grids">
                  <figure className="effect-goliath">
                     <img src={g3} alt="img15"/>
                     <figcaption>
                        <h6>Molten<span>Work</span></h6>
                        <p>Duis posuere ex in mollis iaculis</p>
                        <a href={g3} className="gallery-box" data-lightbox="example-set" data-title="">
                        </a>
                     </figcaption>
                  </figure>
               </div>
               </div>
               </div>
               
               <Link to="/gallery"><img className="next" src={next}></img></Link>
               </section>

          </div>
           {/* top 3 */}
          {/* team*/}
        <section className="team py-lg-4 py-md-3 py-sm-3 py-3" id="team">
          <div className="container py-lg-5 py-md-4 py-sm-4 py-3">
            <div className="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">
              <h3>Team Members</h3>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-6 col-sm-6 team-list-team">
                <div className="team-member">
                  <div className="team-img  circle-img">
                    <img src={T1} alt="" className="image-fluid" />
                  </div>
                  <div className="team-hover">
                    <div className="desk">
                      <h4 className="mb-2">Joya Mily</h4>
                      <p>Manager</p>
                    </div>
                  </div>
                </div>
                <div className="team-title">
                  <h5>Joya Mily</h5>
                  <span>Manager</span>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 team-list-team">
                <div className="team-member">
                  <div className="team-img">
                    <img src={T2} alt="" className="image-fluid" />
                  </div>
                  <div className="team-hover">
                    <div className="desk">
                      <h4 className="mb-2">Max Hammer</h4>
                      <p>Founder & CEO</p>
                    </div>
                  </div>
                </div>
                <div className="team-title">
                  <h5>Max Hammer</h5>
                  <span>founder & ceo</span>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 team-list-team">
                <div className="team-member">
                  <div className="team-img circle-img">
                    <img src={T3} alt="" className="image-fluid" />
                  </div>
                  <div className="team-hover">
                    <div className="desk">
                      <h4 className="mb-2">Jonn Jozz</h4>
                      <p>Site manager</p>
                    </div>
                  </div>
                </div>
                <div className="team-title">
                  <h5>Jonn Jozz</h5>
                  <span>Site Manager</span>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 team-list-team">
                <div className="team-member">
                  <div className="team-img circle-img">
                    <img src={T4} alt="" className="image-fluid" />
                  </div>
                  <div className="team-hover">
                    <div className="desk">
                      <h4 className="mb-2">Ray Rox</h4>
                      <p>Head Manager</p>
                    </div>
                  </div>
                </div>
                <div className="team-title">
                  <h5>Ray Rox</h5>
                  <span>Head Manager</span>
                </div>
              </div>
            </div>
          </div>
        </section>
          {/* team*/}
        </div>
      );
    }
  }
  
  export default Home;