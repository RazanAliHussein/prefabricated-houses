import React, { Component } from "react";
import "./Style/style.css";
import Image from "./images/ab1.jpg";
import "./Style/Blog.css";
import ReadMoreReact from 'read-more-react';


const Articles = ({title, text, date, img}) => {
   return (
     <div>
       <div className="carousel-inner">
         <div className="carousel-item">
           <div className="row">
             <div className="col-lg-6 px-0 blog-w3layouts-row text-center">
               <img
                 src={img}
                 alt=""
                 className="image-fluid"
               />
             </div>
             <div className="news-date mt-3">
               <ul>
                 <li>
                   <span className="far fa-calendar-check" />
                   <a href="#">{date}</a>
                 </li>
                  </ul>
             </div>
             <div className="blog-slider-wls mt-3">
               <h4>
                 <a href="#">{title} </a>
               </h4>
             </div> 
             <div className="col-lg-6 news-agile-text BlogText ">
             <ReadMoreReact text={text}
                min={80}
                ideal={100}
                max={200} />
              </div>
           </div>
         </div>
       </div>
     </div>
   );
 };


class Blog extends Component {
   state = {
      articles_list : [],
     // todos: [2 ,3 ,4 ,5 ,6 ,7 ,8],
      currentPage: 1,
      todosPerPage: 3
    };
    handleClick = (event) => {
      this.setState({
        currentPage: Number(event.target.id)
      });
    }
   
    handlePageChange(pageNumber) {
      console.log(`active page is ${pageNumber}`);
      this.setState({activePage: pageNumber});
    }

   getArticlesList = async () => {
      this.setState({ isLoading: true });
      try {
        const response = await fetch(
          `http://localhost:8080/blog/articles/`
        );
        const answer = await response.json();
        if (answer.success) {
          const articles_list = answer.result;
          this.setState({ articles_list, isLoading: false });
        } else {
          this.setState({ error_message: answer.message, isLoading: false });
        }
      } catch (err) {
        this.setState({ error_message: err.message, isLoading: false });
      }
    };
    componentDidMount() {
      this.getArticlesList();
    }
    render() {
      const { articles_list, currentPage, todosPerPage } = this.state;
      const indexOfLastTodo = currentPage * todosPerPage;
      const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
      const currentTodos = articles_list.slice(indexOfFirstTodo, indexOfLastTodo);
  
      const renderTodos = currentTodos.map((article )=> {

        return(
          <div>
        <Articles key={article.article_id}
        title = {article.article_title}
        text = {article.article_text}
        date = {article.article_date}
        img = {article.image_path}
       /></div>
      )});
      const pageNumbers = [];
      for (let i = 1; i <= Math.ceil(articles_list.length / todosPerPage); i++) {
        pageNumbers.push(i);
      }
  
      const renderPageNumbers = pageNumbers.map(number => {
        return (
          <li
            key={number}
            id={number}
            onClick={this.handleClick}
          >
            {number}
          </li>
        );
      });
  
      return (
         <div className="Blog">
  <br />
       
  
           {/* new and blogs */}
        <section className="blog py-lg-4 py-md-3 py-sm-3 py-3" id="blog">
        <div className="container py-lg-5 py-md-4 py-sm-4 py-3">
           <div className="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">
              <h3>News & Blog</h3>
           </div>

           {/* <Articles key={1}
            title = {"Test Title"}
            text = {"test Text Text Text"}
            date = {"10/20/18"}
            img = {"../../public/images/default/350x250.png"}

           /> */}

           {/* {this.state.articles_list.map(article => 
            <Articles key={article.article_id}
            title = {article.article_title}
            text = {article.article_text}
            date = {article.article_date}
            img = {article.image_path}
           />
           )} */}

          
           </div>
           </section>
              {/* new and blogs */}
              <div>
            <ul>
              {renderTodos}
            </ul>
            <ul id="page-numbers">
              {renderPageNumbers}
            </ul>
          </div>
       </div>
      );
   }
}
    
export default Blog;
