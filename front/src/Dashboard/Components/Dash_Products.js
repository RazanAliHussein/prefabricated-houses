import React, { Component } from "react";
class Dash_Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibility: false,
      product_list:[],
      error_message:"",
      title:"",
      price:"",
      image_id:"",
      description:"",
      Id:"",
      category_id:""
    };
    this.toggleVisibility = this.toggleVisibility.bind(this);
  }
  toggleVisibility() {
    this.setState({
      visibility: !this.state.visibility
    });
  }
  //get all products
  getProductList = async() => {
   try {
     const response = await fetch(`http://localhost:8080/store/products?filter=all`);
     const answer = await response.json();
     if (answer.success) {
       const product_list = answer.result;
       this.setState({ product_list });
     } else {
       const error_message = answer.message;
       this.setState({ error_message });
     }
   } catch (err) {
     this.setState({ error_message: err.message });
   }
 };
  //get all products end
  //get product according to Id
  getProduct = async id => {
   // check if we already have the product
   const previous_product = this.state.product_list.find(
     product => product.product_id === id
   );
   if (previous_product) {
     return; // do nothing, no need to reload a contact we already have
   }
   try {
     const response = await fetch(`http://localhost:8080/store/product/${this.state.Id}`);
     const answer = await response.json();
     if (answer.success) {
       // add the user to the current list of contacts
       const product = answer.result;
       console.log(product);
       const description=(product.product_description);
       this.setState({ description});
       const title=(product.product_title);
       this.setState({ title });
       const price= (product.product_price);
       this.setState({ price });
       const category_id=(product.category_id);
       this.setState({ category_id });

       const product_list = [...this.state.product_list, product];
       this.setState({ product_list });
     } else {
       this.setState({ error_message: answer.message });
     }
   } catch (err) {
     this.setState({ error_message: err.message });
   }
 
 };
  //get product according to Id end
  //create new product
  createProduct = async props => {
   try {
     if (
       !props ||
       !(props.title && props.description && props.price  && props.category_id)
     ) {
       throw new Error(`you need to fill all the fields`);
     }
    
     const { title, description, price,category_id } = props;
     const response = await fetch(
       `http://localhost:8080/product/new?title=${title}&description=${description}&price=${price}&category_id=${category_id}`
     );
  
     const answer = await response.json();
     if (answer.success) {
       // we reproduce the user that was created in the database, locally
       //const id = answer.result;
       const product = { title,  description, price,category_id };
       const product_list = [...this.state.articles_list, product];
       this.setState({ product_list });
      
     } else {
       this.setState({ error_message: answer.message });
     }
   } catch (err) {
     this.setState({ error_message: err.message });
   }
 };

  //create new product end
  //delete product
  deleteProduct = async id => {
   try {
     const response = await fetch(
       `http://localhost:8080/product/delete/${this.state.Id}`
     );
     window.alert("Product deleted")
     const answer = await response.json();
     if (answer.success) {
       // remove the user from the current list of users
       const product_list = this.state.contacts_list.filter(
         product => product.id !== id
       );
       this.setState({ product_list });
     } else {
       this.setState({ error_message: answer.message });
     }
   } catch (err) {
     this.setState({ error_message: err.message });
   }
 };
  //delete product end
  componentDidMount() {
   this.getProductList();
  
 }
 onSubmit = (evt) => {
   // extract name and email from state
   const {  title,  description,price, category_id
  } = this.state
  //console.log(title, description, price, category_id);
   // create the contact from mail and email
   this.createProduct({ title,  description,price, category_id
     })
   // empty name and email so the text input fields are reset
   this.setState({ title:"", 
   price:"", 
   description:"",
   category_id :""
  })
  window.alert("New Row Inserted")
 }
  render() {
      return (
        <div>
          <button
            className="block"
            style={{ background: "red", color: "white" }}
            onClick={this.toggleVisibility}            
            >
            Product
          </button>

        { this.state.visibility ? ( <div>
    <div className="container">
       <div>
          <div className="dash_general">
             <div>
                
                   <br />
                   <br />
                   <button className="ballout" onClick={() => this.getProduct()}>Get</button>
                   <button className="ballout" onClick={() => this.deleteProduct()}>Delete</button>
                
             </div>
             {/*Component starts here */}
          </div>
          <form  onSubmit={this.onSubmit}>
          <div className="row">
                <div className="col-25">
                   <label for="fname">Id</label>
                </div>
                <div className="col-75">
                   <input
                      type="text"
                      id="fname"
                      name="firstname"
                      placeholder="Id"
                      onChange={(evt)=> this.setState({Id:evt.target.value})} 
                      value={this.state.Id}
                      />
                </div>
             </div>
             <div className="row">
                <div className="col-25">
                   <label for="fname">Title</label>
                </div>
                <div className="col-75">
                   <input
                      type="text"
                      id="fname"
                      name="firstname"
                      placeholder="Title."
                      onChange={(evt)=> this.setState({title:evt.target.value})} 
                      value={this.state.title}
                      />
                </div>
             </div>
             <div className="row">
                <div className="col-25">
                   <label for="fname">category_id</label>
                </div>
                <div className="col-75">
                   <input
                      type="text"
                      id="fname"
                      name="firstname"
                      placeholder="category id"
                      onChange={(evt)=> this.setState({category_id:evt.target.value})} 
                      value={this.state.category_id}
                      />
                </div>
             </div>
             <div className="row">
                <div className="col-25">
                   <label for="fname">Price</label>
                </div>
                <div className="col-75">
                   <input
                      type="number"
                      id="fname"
                      name="firstname"
                      placeholder="Price in USD."
                      onChange={(evt)=> this.setState({price:parseFloat(evt.target.value)})} 
                      value={this.state.price}
                      />
                </div>
             </div>
             {/* <div className="row">
                <div className="col-25">
                   <label for="lname">Image</label>
                </div>
                <div className="col-75">
                   <input
                      type="text"
                      id="lname"
                      name="lastname"
                      placeholder="Select Image If You Want "
                      />
                   <input
                      input
                      className="save"
                      type="button"
                      value="Browse"
                      />
                </div>
             </div> */}
             <div className="row">
                <div className="col-25">
                   <label for="subject">Description</label>
                </div>
                <div className="col-75">
                   <textarea
                   id="subject"
                   name="subject"
                   placeholder="Write something.."
                   style={{ height: 200 }}
                   onChange={(evt)=> this.setState({description:evt.target.value})} 
                   value={this.state.description}
                   />
                   <div className="row" />
                      {/* <input input  className="save"
                         type="button"  value="Save"/> */}
                      <input
                         className="col-75"
                         type="submit"
                         value="Submit"
                         onClick={() => this.createProduct()}
                         />
                         <input className="col-75" type="submit" value="Update" onClick={() => this.updateProduct()}/>
                   </div>
                </div>
          </form>
          </div>
       </div>
    </div>    ) : null }
        </div>
      );
  }
}

export default Dash_Products;
