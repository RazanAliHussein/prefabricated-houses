import React, { Component } from "react";

const Sliders = () => {
  return (
    <div className="container">
    <div>
       <div className="dash_general">
          <div>
             <form>
                <br />
                <br />
                <button className="ballout">Get</button>
                <button className="ballout">Delete</button>
             </form>
          </div>
          {/*Component starts here */}
       </div>
       <form action="/action_page.php">
          <div className="row">
             <div className="col-25">
                <label for="fname">Title</label>
             </div>
             <div className="col-75">
                <input
                   type="text"
                   id="fname"
                   name="firstname"
                   placeholder="Slider Title."
                   />
             </div>
          </div>
          <div className="row">
             <div className="col-25">
                <label for="lname">Image</label>
             </div>
             <div className="col-75">
                <input
                   type="text"
                   id="lname"
                   name="lastname"
                   placeholder="Select Image"
                   />
                <input
                   input
                   className="save"
                   type="button"
                   value="Browse"
                   />
             </div>
          </div>
          <div className="row">
             <div className="col-25">
                <label for="subject">Description </label>
             </div>
             <div className="col-75">
                <textarea
                id="subject"
                name="subject"
                placeholder="Write something.."
                style={{ height: 200 }}
                />
                <div className="row" />
                   {/* <input input  className="save"
                      type="button"  value="Save"/> */}
                   <input
                      className="col-75"
                      type="submit"
                      value="Submit"
                      />
                </div>
             </div>
       </form>
       </div>
    </div>    
  );
}

class Dash_Sliders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibility: false
    };
    this.toggleVisibility = this.toggleVisibility.bind(this);
  }
  toggleVisibility() {
    this.setState({
      visibility: !this.state.visibility
    });
  }
  render() {
        return (
          <div>
            <button className="block" onClick={this.toggleVisibility}>
              Slider
            </button>
            <div>
              {this.state.visibility ? <Sliders /> : null}
            </div>
          </div>
        );
  }
}

export default Dash_Sliders;
