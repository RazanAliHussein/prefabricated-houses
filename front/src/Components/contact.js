import React, { Component } from "react";
import Style from "./Style/style.css";
import { Link } from "react-router-dom";
import contactstyle from "./Style/contact.css";

class Contact extends Component {
  render() {
    return (
      <div className="Contact">
         <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        
        {/* contact  */}
        <section className="contacts py-lg-4 py-md-3 py-sm-3 py-3">
          <div className="container py-lg-5 py-md-4 py-sm-4 py-3">
            <div className="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">
              <h3>Contact Us</h3>
            </div>
            <div className="agile-info-para">
              {/* contact-map  */}
              <form action="#" method="post">
                <div className="row agile-wls-contact-mid">
                  <div className="userdata col-md-6 form-group contact-forms">
                    <input
                      type="text"
                      className="form-control "
                      placeholder="Name"
                      required=""
                    />
                  </div>
                  <div className="userdata col-md-6 form-group contact-forms">
                    <input
                      type="email"
                      className="form-control "
                      placeholder="Email"
                      required=""
                    />
                  </div>
                </div>
                <div className="form-group contact-forms">
                  <textarea
                    className="form-control"
                    rows="3"
                    placeholder="Message.."
                    required=""
                  />
                </div>
                <div>
                  <label>
                    Please choose the branch that you want contact them:
                  </label>
                  <br />
                  <select name="Branch" className="btn click-me">
                    <option value="Lebanon">Lebanon</option>
                    <option value="France">France</option>
                    <option value="Canada">Canada</option>
                  </select>
                </div>
                <div className="text-left click-subscribe">
                  <button type="submit" className="btn click-me">
                    Send
                  </button>
                </div>
              </form>
            </div>
            {/* contact-map 
            contact-form */}
            <table>
              <div className="row contact-right mt-lg-5 mt-md-4 mt-3">
                <tr>
                  <td>
                  <iframe src="https://www.google.com/maps/d/embed?mid=17WqbESajFnu9bAeZDwwjZg23xdE"></iframe>
                  </td>
                  <td>
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11379667.893494574!2d-6.924094218123403!3d45.8665231138562!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd54a02933785731%3A0x6bfd3f96c747d9f7!2sFrance!5e0!3m2!1sen!2slb!4v1543486926257"></iframe>
                  </td>
                  <td>
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d53452005.28793382!2d-157.65036949154552!3d35.14535913074742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b0d03d337cc6ad9%3A0x9968b72aa2438fa5!2sCanada!5e0!3m2!1sen!2slb!4v1543487046904"></iframe>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div className="col-lg-4 col-md-5 footer_grid_left text-center">
                      <div className="contact_footer_grid_left text-center mb-3">
                        <span className="fas fa-map-marker-alt" />
                      </div>

                      <p>
                        348 Melbourne, Beverly Hills, <br />
                        New York City 90210.
                      </p>
                    </div>
                  </td>
                  <td>
                    <div className="col-lg-4 col-md-5 footer_grid_left text-center">
                      <div className="contact_footer_grid_left text-center mb-3">
                        <span className="fas fa-map-marker-alt" />
                      </div>

                      <p>
                        348 Melbourne, Beverly Hills, <br />
                        New York City 90210.
                      </p>
                    </div>
                  </td>
                  <td>
                    <div className="col-lg-4 col-md-5 footer_grid_left text-center">
                      <div className="contact_footer_grid_left text-center mb-3">
                        <span className="fas fa-map-marker-alt" />
                      </div>

                      <p>
                        348 Melbourne, Beverly Hills, <br />
                        New York City 90210.
                      </p>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div className="col-lg-4 col-md-3 footer_grid_left text-center">
                      <div className="contact_footer_grid_left text-center mb-3">
                        <span className="fas fa-phone-volume" />
                      </div>
                      <p>
                        +(000) 123 4565 32
                        <br />
                      </p>
                    </div>
                  </td>
                  <td>
                    <div className="col-lg-4 col-md-3 footer_grid_left text-center">
                      <div className="contact_footer_grid_left text-center mb-3">
                        <span className="fas fa-phone-volume" />
                      </div>
                      <p>
                        +(000) 123 4565 32
                        <br />
                      </p>
                    </div>
                  </td>
                  <td>
                    <div className="col-lg-4 col-md-3 footer_grid_left text-center">
                      <div className="contact_footer_grid_left text-center mb-3">
                        <span className="fas fa-phone-volume" />
                      </div>
                      <p>
                        +(000) 123 4565 32
                        <br />
                      </p>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td>
                    <div className="col-lg-4 col-md-4 footer_grid_left text-center">
                      <div className="contact_footer_grid_left text-center mb-3">
                        <span className="fas fa-at" />
                      </div>
                      <p>
                        <a href="mailto:info@example.com">info@example1.com</a>
                      </p>
                    </div>
                  </td>
                  <td>
                    <div className="col-lg-4 col-md-4 footer_grid_left text-center">
                      <div className="contact_footer_grid_left text-center mb-3">
                        <span className="fas fa-at" />
                      </div>
                      <p>
                        <a href="mailto:info@example.com">info@example1.com</a>
                      </p>
                    </div>
                  </td>
                  <td>
                    <div className="col-lg-4 col-md-4 footer_grid_left text-center">
                      <div className="contact_footer_grid_left text-center mb-3">
                        <span className="fas fa-at" />
                      </div>
                      <p>
                        <a href="mailto:info@example.com">info@example1.com</a>
                      </p>
                    </div>
                  </td>
                </tr>
              </div>
            </table>
          </div>
        </section>
        {/* contact   */}
      </div>
    );
  }
}
export default Contact;
