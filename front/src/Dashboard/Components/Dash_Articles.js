import React, { Component } from "react";

class Dash_Article extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibility: false,
      articles_list: [],
      error_message: "",
      title:"", 
      text:"", 
      image_id :"",
      Id:"",
      owner:""
    };
    this.toggleVisibility = this.toggleVisibility.bind(this);
  }
  toggleVisibility() {
    this.setState({
      visibility: !this.state.visibility
    });
  }

  getArticleList = async() => {
    try {
      const response = await fetch(`http://localhost:8080/blog/articles/`);
      const answer = await response.json();
      if (answer.success) {
        const articles_list = answer.result;
        this.setState({ articles_list });
      } else {
        const error_message = answer.message;
        this.setState({ error_message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  //get Article
  getArticle = async Id => {
    // check if we already have the contact
    const previous_article = this.state.articles_list.find(
      article => article.article_id === Id
    );
    
    if (previous_article) {
     
      return; // do nothingp, no need to reload a contact we already have
    }
    try {
      const response = await fetch(`http://localhost:8080/blog/articles/${this.state.Id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of contacts
        const article = answer.result;
        const owner=(article[0].article_owner);
        this.setState({ owner });
        const title=(article[0].article_title);
        this.setState({ title });
        const text=(article[0].article_text);
        this.setState({ text });
        const  image_id =(article[0].image_path);
        this.setState({ image_id });
        const articles_list = [...this.state.articles_list, article];
        this.setState({ articles_list });
        console.log("get it");
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }

  };
  //get Article end
  //delete article
  deleteArticle = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/blog/article/delete/${this.state.Id}`
      );
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const articles_list = this.state.contacts_list.filter(
          article => article.id !== id
        );
        this.setState({ articles_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
    window.alert("Deleted");
  };

  //delete article end

  //update article
  // title:"", 
  // text:"", 
  // image_id :"",
  // Id:"",
  // owner:""
  updateArticle = async (Id, props) => {
   
    const { title, image_id, text, owner } = props;
    const response = await fetch(
      `http://localhost:8080/blog/article/update/${Id}?title=${title}&text=${text}&owner=${owner}&image_id=${image_id}`
    );
    const answer = await response.json();
    if (answer.success) {
      // we update the user, to reproduce the database changes:
      const articles_list = this.state.articles_list.map(article => {
        // if this is the contact we need to change, update it. This will apply to exactly
        // one contact
        if (article.article_id=== Id) {
          const new_article = {
            Id:article.article_id,
            owner:article.article_owner,
            image_id :article.image_path,
            text: article.article_text,
            title:article.article_title
          };
          console.log("return")
          return new_article;
        }
        // otherwise, don't change the contact at all
        else {
          return article;
        }
      });
      this.setState({ articles_list });
    } else {
      this.setState({ error_message: answer.message });
    }
  
  };
  //update article end
  //create article
  createArticle = async props => {
    try {
      if (
        !props ||
        !(props.title && props.text && props.owner && props.image_id)
      ) {
        throw new Error(`you need to fill all the fields`);
      }
      const { title, text, owner, image_id } = props;
      const response = await fetch(
        `http://localhost:8080/blog/article/new?title=${title}&text=${text}&owner=${owner}&image_id=${image_id}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const article = { title, text, owner, image_id };
        const articles_list = [...this.state.articles_list, article];
        this.setState({ articles_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  //create article end
  

  componentDidMount() {
    this.getArticleList();
   
  }
  onSubmit = (evt) => {
    // extract name and email from state
    const {  title, 
    text, 
    owner,
    image_id,
   } = this.state
    // create the contact from mail and email
    this.createArticle({ title,text, 
      owner,
      image_id
      })
    // empty name and email so the text input fields are reset
    this.setState({ title:"", 
    text:"", 
    owner:"",
    image_id :"",
    Id:""})

  }

  render() {
    const { contacts_list, error_message } = this.state;

    return (
      <div>
        {error_message ? <p> ERROR! {error_message}</p> : false}
        <button
          className="block"
          style={{ background: "green", color: "white" }}
          onClick={this.toggleVisibility}
        >
          Articles
        </button>
        {this.state.visibility ? (
          // <Articles
          //   delete={this.deleteArticle}
          //   create={this.createArticle}
          //   update={this.updateArticle}
          //   get={this.getArticle}
          // />
          <div>
          <div className="container">
            <div>
              <div className="dash_general">
                <div>
                  
                    <br />
                    <br />
                    <button className="ballout" onClick={() => this.getArticle()}>Get</button>
                    <button className="ballout" onClick={() => this.deleteArticle()}>Delete</button>
                  
                </div>
                {/*Component starts here */}
              </div>
              {/* <form action="/action_page.php"> */}
           <form  onSubmit={this.onSubmit}>
                <div className="row">
                <div className="col-25">
                    <label for="fname">ID</label>
                  </div>
                  <div className="col-25">
                    <input
                      type="text"
                      id="fname"
                      name="firstname"
                      placeholder=" ID"
                      onChange={(evt)=> this.setState({Id:evt.target.value})} 
                      value={this.state.Id}
                    />
                  </div>
                  </div>
                  <div className="row">
                  <div className="col-25">
                    <label for="fname">Author</label>
                  </div>
                  <div className="col-75">
                    <input
                      type="text"
                      id="fname"
                      name="firstname"
                      placeholder=" Author"
                      onChange={(evt)=> this.setState({owner:evt.target.value})} 
                      value={this.state.owner}
                    />
                  </div>
                </div>
                  <div className="row">
                  <div className="col-25">
                    <label for="fname">Title</label>
                  </div>
                  <div className="col-75">
                    <input
                      type="text"
                      id="fname"
                      name="firstname"
                      placeholder=" title"
                      onChange={(evt)=> this.setState({title:evt.target.value})} 
                      value={this.state.title}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-25">
                    <label for="lname">Image</label>
                  </div>
                  <div className="col-75">
                    <input
                      type="text"
                      id="lname"
                      name="lastname"
                      placeholder="Select Image If You Want "
                      onChange={(evt)=> this.setState({image_id:evt.target.value})} 
                      value={this.state.image_id}
                    />
                    <input input className="save" type="button" value="Browse" />
                  </div>
                </div>
                <div className="row">
                  <div className="col-25">
                    <label for="subject">Subject</label>
                  </div>
                  <div className="col-75">
                    <textarea
                      id="subject"
                      name="subject"
                      placeholder="Write something.."
                      onChange={(evt)=> this.setState({text:evt.target.value})} 
                      value={this.state.text}
                      style={{ height: 200 }}
                    />
                    <div className="row" />
                    {/* <input input  className="save"
                    type="button"  value="Save"/> */}
                    <input className="col-75" type="submit" value="Update" onClick={() => this.updateArticle()}/>
                    <input className="col-75" type="submit" value="Create" onClick={() => this.createArticle()} />
                  </div>
                </div>
                </form>
            </div>
          </div>
        </div>
    
        ) : null}
      </div>
    );
  }
}

export default Dash_Article;
