import Express from 'express'
import upload from '../upload'

const upload_limit = 50;

const app = Express()

export default async (controller) => {

  app.get('/', (req, res, next) => res.send("ok from images"));

  // Upload Multiple Images
  // Route : /images/upload/multi > Body how do i send body and file request ?
  app.post('/upload/multi', upload.array('image', upload_limit), async (req, res, next) => {
    try {
      const {
        width,
        height
      } = req.query;
      const image = req.files;
      const result = await controller.createImage({ image, width, height });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // Upload Single Images
  // Route : /images/upload/single > Body how do i send body and file request ?
  app.post('/upload/single', upload.single('image'), async (req, res, next) => {
    try {
      const {
        width,
        height
      } = req.query;
      const image = req.file;
      const result = await controller.createImage({ image, width, height });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // Register Images Products
  // Route : /images/product/new?imgID=0&prodID=0&def=0
  app.get('/product/new', async (req, res, next) => {
    try {
      const {
        imgID,
        prodID,
        def
      } = req.query;
      const result = await controller.createProductImages({ imgID, prodID, def });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // Delete Images Product
  // @@ TODO
  // app.get("/product/del/:id", async (req, res, next) => {
  //   try {
  //     const {
  //       imgID,
  //       prodID,
  //       def
  //     } = req.query;
  //     const result = await controller.createProductImages({ imgID, prodID, def });
  //     res.json({ success: true, result });
  //   } catch (e) {
  //     next(e);
  //   }
  // });

  return app
}