import React, { Component } from "react";
import "./Style/style.css";
import "./Style/about.css";
import T1 from "./images/t1.jpg";
import T2 from "./images/t2.jpg";
import T3 from "./images/t3.jpg";
import T4 from "./images/t4.jpg";

class About extends Component {
  render() {
    return (
      <div className="About">
          <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        
        {/* about  */}

        <section className="about-inner py-lg-4 py-md-3 py-sm-3 py-3">
          <div className="container-fluid py-lg-5 py-md-4 py-sm-4 py-3">
            <div className="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">
              
            </div>
            <div className="row">
              <div className="col-lg-4 col-md-4 w3layouts-right-side-img aboutparts">
                
                <div className="abut-inner-wls-head">
                 
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Vestibulum nibh urna, euismod ut ornare non, volutpat vel
                    tortor. Integer laoreet placerat suscipit. Sed sodales
                    scelerisque commodo. Nam porta
                  </p>
                </div>
              </div>
             

            </div>
          </div>
        </section>
        {/* about */}

        {/* team*/}
        <section className="team py-lg-4 py-md-3 py-sm-3 py-3" id="team">
          <div className="container py-lg-5 py-md-4 py-sm-4 py-3">
            <div className="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">
              <h3>Team Members</h3>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-6 col-sm-6 team-list-team">
                <div className="team-member">
                  <div className="team-img">
                    <img src={T1} alt="" className="image-fluid" />
                  </div>
                  <div className="team-hover">
                    <div className="desk">
                      <h4 className="mb-2">Joya Mily</h4>
                      <p>Manager</p>
                    </div>
                  </div>
                </div>
                <div className="team-title">
                  <h5>Joya Mily</h5>
                  <span>Manager</span>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 team-list-team">
                <div className="team-member">
                  <div className="team-img">
                    <img src={T2} alt="" className="image-fluid" />
                  </div>
                  <div className="team-hover">
                    <div className="desk">
                      <h4 className="mb-2">Max Hammer</h4>
                      <p>Founder & CEO</p>
                    </div>
                  </div>
                </div>
                <div className="team-title">
                  <h5>Max Hammer</h5>
                  <span>founder & ceo</span>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 team-list-team">
                <div className="team-member">
                  <div className="team-img">
                    <img src={T3} alt="" className="image-fluid" />
                  </div>
                  <div className="team-hover">
                    <div className="desk">
                      <h4 className="mb-2">Jonn Jozz</h4>
                      <p>Site manager</p>
                    </div>
                  </div>
                </div>
                <div className="team-title">
                  <h5>Jonn Jozz</h5>
                  <span>Site Manager</span>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-6 team-list-team">
                <div className="team-member">
                  <div className="team-img">
                    <img src={T4} alt="" className="image-fluid" />
                  </div>
                  <div className="team-hover">
                    <div className="desk">
                      <h4 className="mb-2">Ray Rox</h4>
                      <p>Head Manager</p>
                    </div>
                  </div>
                </div>
                <div className="team-title">
                  <h5>Ray Rox</h5>
                  <span>Head Manager</span>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
export default About;
