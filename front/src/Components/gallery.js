import React, { Component } from "react";
import "./Style/style.css";
import { Link } from "react-router-dom";
import './Style/gallery.css'
const Store = ({type,price,img}) => {
  return(
    <div className="galleries">
    
      <figure className="effect-goliath">
               <img src= {require('./images/g1.jpg')} alt="img15" />
               <figcaption>
                 <h6>
                  {type}
                 </h6>
                 <h5>{price}</h5>
                 <Link to="/gallery/item/:id" className="btn click-me">
                   more..
                 </Link>
               </figcaption>
       </figure>
    </div>
  );
};

class Gallery extends Component {
  state = {
    list: [
      { name: "Tyler", friend: true },
      { name: "Ryan", friend: true },
      { name: "Michael", friend: false },
      { name: "Mikenzie", friend: false },
      { name: "Jessica", friend: true },
      { name: "Dan", friend: false }
    ],
    listSelected: "all",
    products_list : []
  };
  renderList = () => {
    if (this.state.listSelected === "all") {
      return (
        <div>
          <h1>All:</h1>
          <ul>
            {this.state.list.map(function(user) {
              return <li key={user.name}>{user.name}</li>;
            })}
          </ul>
        </div>
      );

    } else if (this.state.listSelected === "friends") {
      const friends=this.state.list.filter(function(user) {
        return user.friend === true;
      });
      return (
        <div>
          <h1>Friends:</h1>
          <ul>
            {friends.map(function(user) {
              return <li key={user.name}>{user.name}</li>;
            })}
          </ul>
        </div>
      );
    } 
    
    
    else if (this.state.listSelected === "nonfriends") {
      const nonFriends = this.state.list.filter(function(user) {
        return user.friend !== true;
      });
      return (
        <div>
          <h2>Non friends</h2>
          <ul>
              {nonFriends.map(function (user) {
               return <li key={user.name}>{user.name}</li>
             })}
          </ul>
        </div>
      );
    } else {
      return <div>Error</div>;
    }
  };
  FilterAll = () => {
  
    this.setState({ listSelected: "all" });
  };
  FilterFriends = () => {
 
    this.setState({ listSelected: "friends" });
  };

  FilterNonFriends = () => {
 
    this.setState({ listSelected: "nonfriends" });
  };
 
   
    
 

 getStoreProducts = async () => {
    this.setState({ isLoading: true });
    try {
      const response = await fetch(
        `http://localhost:8080/store/products/?filter=all`
      );
      const answer = await response.json();
      if (answer.success) {
        const products_list = answer.result;
        this.setState({ products_list, isLoading: false });
      } else {
        this.setState({ error_message: answer.message, isLoading: false });
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading: false });
    }
  };
  componentDidMount() {
    this.getStoreProducts();
  }

  render() {
    return (
      <div className="Gallery">
        {/* gallery */}
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />products_list
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
       
         
            <div className="title ">
              <h3>Our Gallery</h3>
            </div>
          
            <div>
            <form className="galleryFilter">
              <button className="btn click-me" onClick={this.FilterAll}>All</button>
              <button className="btn click-me" onClick={this.FilterFriends}>>M1</button>
              <button className="btn click-me" onClick={this.FilterNonFriends}>M3</button>
              <button className="btn click-me" >M5</button>
            </form>
            </div>
            
              {this.state.products_list.map(product => 
            <Store className="Gallery-Item" key={product.product_id}
            type = {product.category_id} // All - 0 - 1 - 2 - 3 < 6
            price = {product.product_price}
            // img = {"public/images/default/350x250.png"}
            // product.product_path
           />
           )} 

        {/* Gallery	 */}

      </div>
    );
  }
}
export default Gallery;
