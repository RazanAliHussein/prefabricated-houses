import React, { Component } from "react";
import { Link } from "react-router-dom";
import dash_general from './Style/dash_general.css'
import login_css from './Style/login.css'
import ComponentSlider from "./Components/Dash_Sliders";
import ComponentImages from "./Components/Dash_Images";
import ComponentArticle from "./Components/Dash_Articles";
import ComponentProduct from "./Components/Dash_Products";
import { makeRequestUrl } from "./utils.js";

const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);

class Dash extends Component {
  state = {
    token:null,
    nick:null
  }
  login = async (username, password) => {
    try {
      const url = makeUrl(`login`, {
        username,
        password,
        token: this.state.token
      });
      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        const { token, nick } = answer.result;
        this.setState({ token, nick });
        // toast(`successful login`);
      } else {
        this.setState({ error_message: answer.message });
        // toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      // toast.error(err.message);
    }
  };
  logout = async token => {
    try {
      const url = makeUrl(`logout`, { token: this.state.token });
      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        this.setState({ token: null, nick: null });
        // toast(`successful logout`);
      } else {
        this.setState({ error_message: answer.message });
        // toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      // toast.error(err.message);
    }
  };
  onLoginSubmit = evt => {
    evt.preventDefault();
    const username = evt.target.username.value;
    const password = evt.target.password.value;
    if (!username) {
      alert("username can't be empty");
     // toast.error("username can't be empty");
      return;
    }
    if (!password) {
      alert("password can't be empty");
     // toast.error("password can't be empty");
      return;
    }
    this.login(username, password);
  };

  renderUserLoggedOut() {
    return (
      <div>
         <div className="Dash">
            <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
            <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
            <br /> <br /> <br /> <br /> <br />
            </div>
      <div class="container">
      <form className="third" onSubmit={this.onLoginSubmit}>
      <label for="uname"><b>Username</b></label>
        <input name="username" placeholder="username" type="text" />
        <label for="psw"><b>Password</b></label>
        <input name="password" placeholder="password" type="password" />
        <button class="loginbutton" type="submit">Login</button>
      </form>
      </div>
      </div>
    );
  }
  renderUserLoggedIn() {
    return (
      <div>
         <div className="Dash">
            <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
            </div>
            <div>
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <ComponentSlider />
              <br />
              <ComponentImages />
              <br />
              <ComponentArticle />
              <br />
              <ComponentProduct />
              <br />
              <br />
              <br />
              <button class="cancelbtn" onClick={this.logout}>logout</button>
            </div>
      </div>
    );
  }
  renderUser() {
    const { token } = this.state;
    {console.log(token)}
    if (token) {
      // user is logged in
      return this.renderUserLoggedIn();
    } else {
      return this.renderUserLoggedOut();
    }
  }
  render() {
    return (
    <div className="Dash">
    {this.renderUser()}
    </div>
    );
  }
}
export default Dash;
