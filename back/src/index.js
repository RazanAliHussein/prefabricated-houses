import app from './app'
import initializeDatabase from './db'
import { read } from 'fs';
import imagesControllerApp from './controllers/images'
import articlesControllerApp from './controllers/articles'
import productsControllerApp from './controllers/products'
import sliderControllerApp from './controllers/slider'
import orderControllerApp from './controllers/cart'

import { authenticateUser, logout, isLoggedIn } from './middleware/auth'
// import { authenticateUser, logout, isLoggedIn } from './middleware/auth'

const start = async () => {
  const controller = await initializeDatabase()

  app.get('/', (req, res, next) => res.send("Index.JS || OK"));

  // Can I ?
  const imagesApp = await imagesControllerApp(controller);
  app.use('/images', imagesApp);
  // If you see this, then you know what i mean.
  const articlesApp = await articlesControllerApp(controller);
  app.use('/blog', articlesApp);
  // Most probably yes
  
  const productsApp = await productsControllerApp(controller);
  app.use('/', productsApp);

  const sliderApp = await sliderControllerApp(controller);
  app.use('/slider', sliderApp);

  const ordersApp = await orderControllerApp(controller);
  app.use('/orders', ordersApp)
  

  app.get('/login', authenticateUser)
  app.get('/logout', logout)
  app.get('/mypage', isLoggedIn, ( req, res ) => {
    const username = req.user.username
    res.send({success:true, result: 'ok, user '+username+' has access to this page'})
  })

  app.use((err, req, res, next) => {
    console.error(err)
    const message = err.message
    res.status(500).json({
      success: false,
      message
    })
  })

  app.listen(8080, () => console.log('server listening on port 8080'))
}

start();


/* TEMP - API */ 
/* STORE - PRODUCTS */
/*
http://localhost:8080/store/product/1
http://localhost:8080/store/products?filter=all
http://localhost:8080/store/products?filter=1
http://localhost:8080/store/products?filter=2
http://localhost:8080/store/products?filter=3
http://localhost:8080/store/products?filter=4
http://localhost:8080/store/products?filter=5

http://localhost:8080/product/new?title=value&description=value&price=value&category_id=0
http://localhost:8080/product/1/images/
http://localhost:8080/product/delete/1
http://localhost:8080/product/update/1?title=value&description=value&price=value&category_id=0
*/

/* IMAGES - UPLOADS */
/*
http://localhost:8080/images/upload/mutli?width=250&height=300  |!| Post Request
http://localhost:8080/images/upload/single?width=250&height=300  |!| Post Request
http://localhost:8080/images/product/new?imgID=0&prodID=0&def=0
*/

/* ARTICLES */
/*
http://localhost:8080/blog/articles
http://localhost:8080/blog/articles/1
http://localhost:8080/blog/article/new?title=Title&text=DDD&owner=Admin&image_id=0
http://localhost:8080/blog/article/update/1?title=value&text=value&owner=value&image_id=0
*/

/* SLIDERS */
/* 
http://localhost:8080/slider/new?image_id=value&title=value&text=value
http://localhost:8080/slider/update/id?image_id=0&title=value&text=value
http://localhost:8080/slider/delete/1
http://localhost:8080/slider/data
*/