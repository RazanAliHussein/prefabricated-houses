import React, { Component } from "react";
import g1 from "./images/g1.jpg";
import g2 from "./images/g2.jpg";
import g3 from "./images/g3.jpg";
import g4 from "./images/g4.jpg";
import "./Style/Item.css";
import "./Style/style.css";
import "./Style/contact.css";


const OneItems = ({ItemDescription, Itemprice,AddtoCart}) => {
  return (
    <div className="ItemContents TwoItem ItemDetails ItemParts">
      {/* Item Details */}
      
       
          <h3 className="ItemTitle">Product Details </h3>

          <div className="ItemDesc">{ItemDescription}</div>
          <h4 className="ItemTitle">Price: {Itemprice}</h4>
          <br />
          <br />
          <br />
          <button className="btn click-me AddToCart" onClick={AddtoCart}>
            Add to cart
          </button>
        </div>
     
    
  );
};
class Item extends Component {
  OneItem = (ItemDescription, Itemprice) => {
    return (
      <div className="ItemContent TwoItem images">
        {/* Item Details */}
        
          {/* <div className="ItemDetails ItemParts">
            <h3 className="ItemTitle">Product Details </h3>

            <div className="ItemDesc">{this.state.ItemDescription}</div>
            <h4 className="ItemTitle">Price: {this.state.Itemprice}</h4>
            <br />
            <br />
            <br />
            <button className="btn click-me AddToCart" onClick={this.AddtoCart}>
              Add to cart
            </button>
          </div> */}
          
            <img
              src={g1}
              alt="Nature"
              className="itemImage"
              onClick={this.myFunction}
            />

            <img
              src={g2}
              alt="Snow"
              className="itemImage"
              onClick={this.myFunction}
            />

            <img
              src={g3}
              alt="Mountains"
              className="itemImage"
              onClick={this.myFunction}
            />

            <img
              src={g4}
              alt="Lights"
              className="itemImage"
              onClick={this.myFunction}
            />

            <div className="pre">
              <img src={this.state.currentImage} alt="" id="expand" />
            </div>
         
      
        {/* Item Details */}
      </div>
    );
  };
  state = {
    Item_List: [],
    currentImage: { g1 }.g1,
    Images: [{ g1 }.g1, { g2 }.g2, { g3 }.g3, { g4 }.g4],
    // ItemDescription: `blah blah blah blah blah bbbbbbbbbbbbbbbbbf
    // fffffffffffffffffff ffffffffffffffffffff
    // ffffffffffffffffffff fffffffffffffffffffff blah blah blah blah blah bbbbbbbbbbbbbbbbbf
    // fffffffffffffffffff ffffffffffffffffffff
    // ffffffffffffffffffff fffffffffffffffffffff blah blah blah blah blah bbbbbbbbbbbbbbbbbf
    // fffffffffffffffffff ffffffffffffffffffff
    // ffffffffffffffffffff fffffffffffffffffffff blah blah blah blah blah bbbbbbbbbbbbbbbbbf
    // fffffffffffffffffff ffffffffffffffffffff
    // ffffffffffffffffffff fffffffffffffffffffff blah blah blah blah blah bbbbbbbbbbbbbbbbbf
    // fffffffffffffffffff ffffffffffffffffffff
    // ffffffffffffffffffff fffffffffffffffffffff blah blah blah blah blah bbbbbbbbbbbbbbbbbf
    // fffffffffffffffffff ffffffffffffffffffff
    // ffffffffffffffffffff fffffffffffffffffffff blah blah blah blah blah bbbbbbbbbbbbbbbbbf
    // fffffffffffffffffff ffffffffffffffffffff
    // ffffffffffffffffffff fffffffffffffffffffff  `,
    // ItemPrice: "1000$",
    user_cart : [],
    itemId: this.props.match.params.product_id
  };

  myFunction = e => {
    const currentImage = e.target.src;
    this.setState({ currentImage });
  };
  // AddtoCart = e => {
  //   const Items = e.target;
  //   console.log(Items);
    
  //   window.alert("Added to Cart");
  // };
Added = async props => {
    try {
    
      const { itemId } = props;
      const response = await fetch(
        `http://localhost:8080/orders/new?userID=1&prodID=${this.state.itemId}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const itemCart = { itemId };
        const user_cart = [...this.state.user_cart, itemCart];
        this.setState({ user_cart });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
    window.alert("Added to Cart");
  };
  getStoreProducts = async () => {
    this.setState({ isLoading: true });
    try {
      const response = await fetch(
        `http://localhost:8080/store/product/${this.state.itemId}`
      );
      const answer = await response.json();
      if (answer.success) {
        const Item_List = answer.result;
        this.setState({ Item_List, isLoading: false });
      } else {
        this.setState({ error_message: answer.message, isLoading: false });
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading: false });
    }
  };
  getCartForUser = async () => {
    this.setState({ isLoading: true });
    try {
      const response = await fetch(
        `http://localhost:8080/orders/data/1` // Grab the user ID
      );
      const answer = await response.json();
      if (answer.success) {
        const user_cart = answer.result;
        this.setState({ user_cart, isLoading: false });
      } else {
        this.setState({ error_message: answer.message, isLoading: false });
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading: false });
    }
  };
  
 
  //get cart end
  componentDidMount() {
    this.getStoreProducts();
    this.getCartForUser();
  }

  render() {
    // console.log(this.props.match);
    // console.log(this.props.global);
    // console.log(this.props.match.params.product_id);
    // console.log(this.state.itemId);
    const {
      product_id,
      product_description,
      product_title,
      product_price,
      product_stock,
      category_id
    } = this.state.Item_List;
    console.log(product_id);
    console.log(product_description);
    console.log(product_title);
    console.log(product_price);
    console.log(product_stock);
    console.log(category_id);
    return (
      <div className="Item ">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />

        <this.OneItem/>
        
        <OneItems key={product_id}
          ItemDescription={product_description}
          Itemprice={product_price}
          AddtoCart={this.Added}
        />
      </div>
    );
  }
}

export default Item;
