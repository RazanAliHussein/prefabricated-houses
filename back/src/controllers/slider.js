import Express from 'express'

const app = Express()

export default async (controller) => {
  app.get('/', (req, res, next) => res.send("ok from sldier"));

  // UPDATE SLIDER
  // ROUTE : /slider/update/id?image_id=0&title=value&text=value
  app.get("/update/:id", async (req, res, next) => {
    try {
      const {
        id
      } = req.params;
      const {
        image_id,
        title,
        text
      } = req.query;

      const result = await controller.updateSlider(id, {
        image_id,
        title,
        text
      });
      
      res.json({
        success: true,
        result
      });
    } catch (e) {
      next(e);
    }
  });

  // DELETE SLIDE
  // /slider/delete/1,2,3
  app.get("/delete/:id", async (req, res, next) => {
    try {
      const {
        id
      } = req.params;
      const result = await controller.deleteSlider(id);
      res.json({
        success: true,
        result
      });
    } catch (e) {
      next(e);
    }
  });

  // Get All Slides
  // Route : /slider/data
  app.get('/data', async (req, res, next) => {
    try {
      const data = await controller.getSliderData();
      res.json({
        success: true,
        result: data
      });
    } catch (e) {
      next(e);
    }
  });

  // CREATE NEW SLIDE 
  // /slider/new?image_id=value&title=value&text=value

  app.get("/new", async (req, res, next) => {
    try {
      const {
        image_id,
        title,
        text
      } = req.query;
      const result = await controller.createSlider({
        image_id,
        title,
        text
      });
      res.json({
        success: true,
        result
      });
    } catch (e) {
      next(e);
    }
  });

  return app;
}