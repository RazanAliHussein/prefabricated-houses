import React, { Component } from "react";
import Style from "./Style/style.css";
import Image from "./images/ab1.jpg";
import Blogs from "./Style/Blog.css";
import { Link } from "react-router-dom";
import ReadMoreReact from 'read-more-react';


const Articles = ({title, text, date, img}) => {
   return (
     <div>
       <div className="carousel-inner">
         <div className="carousel-item">
           <div className="row">
             <div className="col-lg-6 px-0 blog-w3layouts-row text-center">
               <img
                 src={img}
                 alt=""
                 className="image-fluid"
               />
             </div>
             <div className="news-date mt-3">
               <ul>
                 <li>
                   <span className="far fa-calendar-check" />
                   <a href="#">{date}</a>
                 </li>
                 {/* <li><span className="fas fa-tags"></span><a href="#">5 Tags</a></li>
                              <li><span className="far fa-comments"></span><a href="#">5 Comments</a></li> */}
               </ul>
             </div>
             <div className="blog-slider-wls mt-3">
               <h4>
                 <a href="#">{title} </a>
               </h4>
             </div> 
             <div className="col-lg-6 news-agile-text BlogText ">
             <ReadMoreReact text={text}
                min={80}
                ideal={100}
                max={200} />
              </div>
           </div>
         </div>
       </div>
     </div>
   );
 };


class Blog extends Component {
   state = {
      articles_list : [],
   }

   getArticlesList = async () => {
      this.setState({ isLoading: true });
      try {
        const response = await fetch(
          `http://localhost:8080/blog/articles/`
        );
        const answer = await response.json();
        if (answer.success) {
          const articles_list = answer.result;
          this.setState({ articles_list, isLoading: false });
        } else {
          this.setState({ error_message: answer.message, isLoading: false });
        }
      } catch (err) {
        this.setState({ error_message: err.message, isLoading: false });
      }
    };
    componentDidMount() {
      this.getArticlesList();
    }
    render() {
      return (
         <div className="Blog">
  <br />
       
  
           {/* new and blogs */}
        <section className="blog py-lg-4 py-md-3 py-sm-3 py-3" id="blog">
        <div className="container py-lg-5 py-md-4 py-sm-4 py-3">
           <div className="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">
              <h3>News & Blog</h3>
           </div>

           {/* <Articles key={1}
            title = {"Test Title"}
            text = {"test Text Text Text"}
            date = {"10/20/18"}
            img = {"../../public/images/default/350x250.png"}

           /> */}

           {this.state.articles_list.map(article => 
            <Articles key={article.article_id}
            title = {article.article_title}
            text = {article.article_text}
            date = {article.article_date}
            img = {article.image_path}
           />
           )}

          
           </div>
           </section>
              {/* new and blogs */}
       </div>
      );
   }
}
    
export default Blog;
